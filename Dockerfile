# We will start from a base image of the latest php version. 
# If your project has a specific php version requirement you can just specify that rather than latest (FROM php:7.2)
FROM php:latest

# Run all commands from before_script that we only want to run once

# Update packages
RUN apt-get update -yqq

RUN apt-get install gnupg -yqq

# Install PHP and composer dependencies
# Here you can install any other extension that you need during the test and deployment process

RUN apt-get update && apt-get install -y \
    git \
    curl \
    zip \
    unzip \
    sqlite3 \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    libsqlite3-dev \
    libmcrypt-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libbz2-dev

# Install needed PHP extensions

RUN docker-php-ext-install pdo_mysql pdo_sqlite mbstring exif pcntl bcmath gd

RUN pecl install xdebug mongodb redis

RUN docker-php-ext-enable xdebug mongodb redis

# Set MongoDB extension
RUN echo "extension=mongodb.so" > /usr/local/etc/php/conf.d/docker-php-ext-mongodb.ini

# Set PHP Memory Limit
RUN echo 'memory_limit = -1' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~1.0"

EXPOSE 27017