<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['middleware' => ['localization','request.log']], function () {
    Route::group(['prefix' => 'v1.0'], function () {
        Route::get('checkServiceStatus', function () {
            return  response()->json([
                'result' => "",
                'statusCode' => 200,
                'message' => "success"
            ], 200);
        });

        Route::group(['namespace'=>'Auth'], function () {

            Route::post('register-as-guest', 'RegisterController@createGuestUser');
            Route::post('register/phone-or-email', 'RegisterController@register');
            Route::post('login', 'LoginController@login');
            Route::post('verify-otp', 'OtpController@otpVerify');
            Route::post('send-otp', 'OtpController@sendOtp');
            // Route::post('check_register', 'RegisterController@checkRegister');
        });
    
        Route::middleware('jwt.verify')->group(function () {
            Route::any('chat_auth', 'Auth\AuthController@getUser');
            Route::get('user/{id}', 'Auth\AuthController@getPersonalInfo');

            Route::group(['namespace'=>'Auth'], function () {
                Route::post('refresh-token', 'RefreshTokenController@refresh');
                Route::post('change-password', 'ChangePasswordController@changePassword');
                Route::post('logout', 'LogoutController@logout');
                Route::post('device-token', 'AuthController@getDeviceToken');
            });
        });
    
        Route::group(['namespace'=>'Auth'], function () {
            Route::post('forget-password', 'ForgetPasswordController@forgetPassword');
            Route::middleware('otp.TokenValidation')->group(function () {
                Route::post('new-password', 'ForgetPasswordController@createNewPassword');
            });
        });
        // Route::post('appVersionCheck', 'AppVersionController@checkAppVersion');
    });
});
