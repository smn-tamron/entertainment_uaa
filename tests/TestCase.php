<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\RewardType;
use App\PersonalInformation;
use App\ClientDeviceToken;
use Tymon\JWTAuth\Facades\JWTAuth;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
       
        $this->successStatusCode = config('http_status.success');
        $this->badRequestStatusCode = config('http_status.badRequest');
        $this->createStatusCode = config('http_status.created');
        $this->notFoundStatusCode = config('http_status.notFound');
        $this->successMsg = trans('message.successMsg');
        $this->updateMsg = trans('message.updateSuccessMsg');
        $this->notFoundMsg = trans('message.notFoundMsg');
    }

    public function successJson($result, $statusCode, $message)
    {
        return [
            "result" => $result,
            "statusCode" => $statusCode,
            "message" =>  $message
        ];
    }

    public function failedJson()
    {
        return [
            "result" => null,
            "statusCode" => $this->badRequestStatusCode,
            "message" =>  $this->notFoundMsg
        ];
    }

    public function failedJsonForValidation()
    {
        return [
            "result" => null,
            "statusCode" => $this->badRequestStatusCode,
        ];
    }

    public function createUser($loginType = null, $loginStatus = 'login', $firstStatus = 1, $coin)
    {
        return factory(User::class)->create([
            'name' => $loginStatus == 'register' ? config('mock_data.guest'): config('mock_data.name'),
            'email' => ($loginType == config('mock_data.emailLoginType') || $loginType == config('mock_data.googleLoginType') || $loginType == config('mock_data.appleLoginType')) ? config('mock_data.email1'):null,
            'phone' => ($loginType == config('mock_data.phoneLoginType'))? config('mock_data.phone'):null,
            'otp_code'=> ($loginType ==  config('mock_data.emailLoginType')) ? config('mock_data.otp') : null,
            'otp_attempt_time'=> $loginType == config('mock_data.emailLoginType')  ? now():null,
            'active_status' => $loginStatus == 'register' ?'pending' : 'active',
            'first_status' => $firstStatus == 0 ? $firstStatus : config('enums.enableFirstStatus'),
            'login_type' => $loginType,
            'user_type' => $loginStatus == 'register' ?'registered' : 'guest',
            'wrong_time' => null,
            'facebook_id' => ($loginStatus == 'login' && $loginType == config('mock_data.facebookLoginType'))? config('mock_data.facebookId') : null,
            'email_id' =>  ($loginStatus == 'login' && $loginType == config('mock_data.googleLoginType'))? config('mock_data.googleId') : null,
            'apple_id' => ($loginStatus == 'login' && $loginType == config('mock_data.appleLoginType'))? config('mock_data.appleId') : null,
            'email_verified_at' => null,
            'coin' => $coin,
            'banned_status' => 0,
            'password' => (($loginType ==  config('mock_data.emailLoginType')) || ($loginType == config('mock_data.phoneLoginType'))) ? bcrypt(config('mock_data.password')) : null,
            'remember_token' => config('mock_data.password'),
        ]);
    }

    public function changeActiveStatusWhenOtpVerify($userId)
    {
        $user = User::find($userId);
        $user->active_status = 'active';
        $user->save();
        return $user;
    }

    public function createPersonalInfo($user)
    {
        return PersonalInformation::create($this->cloneToPersonalInfo($user));
    }

    public function cloneToPersonalInfo($user)
    {
        return [
            'client_user_id' => $user->id,
            'name' => $user->name,
            'image' => config('mock_data.fakeProfile'),
            'phone' => ($user->login_type == config('mock_data.phoneLoginType')) ? $user->phone : null,
            'email' => ($user->login_type == config('mock_data.emailLoginType') ||$user->login_type == config('mock_data.googleLoginType') || $user->login_type == config('mock_data.appleLoginType') ) ? $user->email : null,
            'gender' =>  null,
            'date_of_birth' =>  null,
            'bio' => null,
            'address' =>  null,
        ];
    }

    public function createRewardType($type)
    {
        return RewardType::create([
            'game_category_id' => config('enums.gameCategory.slotReel'),
            'type' => $type,
            'reward_coin' => $type == 'guest' ? config('mock_data.rewardType.guest'): config('mock_data.rewardType.newbie'),
        ]);
    }

    public function createClientDeviceToken($userId, $deviceId, $userStatus)
    {
        return ClientDeviceToken::create([
            'client_user_id' => $userId,
            'device_id' => $deviceId,
            'device_token' => config('mock_data.deviceToken'),
            'device_os_type' => 'ios',
            'user_status' => $userStatus,
            'last_activated_at' => now()
        ]);
    }
    
    public function createJWTAuthToken($user)
    {
        return JWTAuth::fromUser($user);
    }

    public function responseJson($resultJson, $statusCode, $message)
    {
        return [
            "result" => $resultJson,
            "statusCode" => $statusCode,
            "message" =>  $message
        ];
    }
}
