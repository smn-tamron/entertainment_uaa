<?php

namespace Tests\Feature;

use Tests\TestCase;

class GuestUserTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testCreateGuestUserSuccessfully()
    {
        $requestBody = [
            'deviceId' => config('mock_data.deviceId'),
            'deviceToken' => config('mock_data.deviceToken'),
            'deviceOsType' => config('mock_data.deviceOsType'),
        ];

        $this->createRewardType(config('enums.rewardType.guest'));
       
        $response = $this->json('POST', 'api/v1.0/register-as-guest', $requestBody);
        $response->assertOk();
        $response->assertJson($this->successJson(
            [
                "user" => [
                        "id" =>  $response['result']['user']['id'],
                        'name' => config('mock_data.guest'),
                        'image' =>  getFileUrlFromAkoneyaMedia(config('mock_data.fakeProfile')),
                        'userType' => config('mock_data.guestLoginStatus'),
                        "initialUser" => true,
                        "rewardCoin"=> config('mock_data.rewardType.guest')
                ],
                "accessToken" => $response['result']['accessToken']
            ],
            $this->successStatusCode,
            $this->successMsg
        ));
    }

    public function testCreateGuestUserFailed_WithInvalidDeviceId()
    {
        $requestBody = [
            'deviceId' => null,
            'deviceToken' => null,
            'deviceOsType' => null,
        ];
        $response = $this->json('POST', 'api/v1.0/register-as-guest', $requestBody);
        $response->assertStatus($this->badRequestStatusCode);
        $response->assertJson($this->failedJsonForValidation());
    }
}
