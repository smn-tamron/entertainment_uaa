<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RefreshTokenTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->reward = $this->createRewardType(config('enums.rewardType.firstRegister'));
        $this->user = $this->createUser(config('mock_data.emailLoginType'),config('mock_data.loginStatus.register'),config('enums.enableFirstStatus'), $this->reward->reward_coin);
        $this->activeUser = $this->changeActiveStatusWhenOtpVerify($this->user->id);
    }

    public function testRefreshSuccessfully()
    {
        $token = $this->createJWTAuthToken($this->activeUser);
        $headers = [ 'Accept' => 'application/json','Authorization' => "Bearer $token"];
        $response = $this->json('POST', 'api/v1.0/refresh-token', [], $headers);
        $responseResult = $response->decodeResponseJson();
        $response->assertOk();
        $response->assertJson(
            $this->successJson(
                [
                    "accessToken" => $responseResult["result"]["accessToken"]
                ],
                $this->successStatusCode,
                trans('message.successMsg')
            )
        );
    }

    public function testRefreshFailed()
    {
        $unknownToken = config('mock_data.unknownToken');
        $headers = [ 'Accept' => 'application/json','Authorization' => "Bearer $unknownToken"];
        $response = $this->json('POST', 'api/v1.0/refresh-token', [], $headers);
        $response->assertJson($this->successJson(null, 40101, trans('message.tokenInvalidOrExpire')));
    }
}
