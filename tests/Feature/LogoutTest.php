<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->reward = $this->createRewardType(config('enums.rewardType.firstRegister'));
        $this->user = $this->createUser(config('mock_data.emailLoginType'),config('mock_data.loginStatus.register'),config('enums.enableFirstStatus'), $this->reward->reward_coin);
        $this->activeUser = $this->changeActiveStatusWhenOtpVerify($this->user->id);
        $this->token = $this->createJWTAuthToken($this->activeUser);
    }

    public function testLogoutSuccessfully()
    {
        $headers = [ 'Accept' => 'application/json','Authorization' => "Bearer $this->token"];
        $response = $this->post("api/v1.0/logout", [], $headers);
        $response->assertOk();
        $response->assertJson($this->successJson(null, $this->successStatusCode, trans('message.successLogout')));
    }

    public function testLogoutFailed()
    {
        $unknownToken = config('mock_data.unknownToken');
        $headers = [ 'Accept' => 'application/json','Authorization' => "Bearer $unknownToken"];
        $response = $this->post("api/v1.0/logout", [], $headers);
        $response->assertJson($this->successJson(null, 40101, trans('message.tokenInvalidOrExpire')));
    }
}
