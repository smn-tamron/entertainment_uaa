<?php

namespace Tests\Feature;

use Tests\TestCase;

class ChangePasswordTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->token = $this->createPassportToken($this->activeUser);
        $this->assertUnauthorized = $this->assertUnauthorized(config('http_status.unauthorized'));
        $this->headers = [ 'Accept' => 'application/json','Authorization' => "Bearer $this->token"];
    }

    /** @test */
    public function changePasswordSuccess()
    {
        $requestData = [
            "id" => $this->user->id,
            "currentPassword" =>  config('mock_data.password'),
            'newPassword'=> config('mock_data.password')."abc",
            'confirmNewPassword'=> config('mock_data.password')."abc"
        ];
        $response = $this->json('POST', 'api/v1.0/profile/change_password', $requestData, $this->headers);
        $response->assertOk();
        $response->assertJson($this->responseJson(null, $this->successStatusCode, trans('message.successChangePassword')));
    }

    /** @test */
    public function changePasswordFailWhenUserIdInvalid()
    {
        $requestData = [
            "id" => 10,
            "currentPassword" =>  config('mock_data.password'),
            'newPassword'=> config('mock_data.password')."abc",
            'confirmNewPassword'=> config('mock_data.password')."abc"
        ];
        $response = $this->json('POST', 'api/v1.0/profile/change_password', $requestData, $this->headers);
        $this->assertUnauthorized;
        $response->assertJson($this->responseJson(null, config('http_status.unauthorized'), trans('message.tokenInvalidOrExpire')));
    }

    /** @test */
    public function changePasswordFailWhenCurrentPaswordInvalid()
    {
        $requestData = [
            "id" => $this->user->id,
            "currentPassword" =>  config('mock_data.password')."abcd",
            'newPassword'=> config('mock_data.password')."abcdef",
            'confirmNewPassword'=> config('mock_data.password')."abcdef"
        ];
        $response = $this->json('POST', 'api/v1.0/profile/change_password', $requestData, $this->headers);
        $this->assertBadRequest;
        $response->assertJson($this->responseJson(null, config('http_status.badRequest'), trans('message.currentPasswordFail')));
    }

    /** @test */
    public function changePasswordValidationFail()
    {
        $requestData = [
            "id" => $this->user->id,
            "currentPassword" =>  config('mock_data.password'),
            'newPassword'=> config('mock_data.password')."abcdef",
            'confirmNewPassword'=> config('mock_data.password')."abcdefgh"
        ];
        $response = $this->json('POST', 'api/v1.0/profile/change_password', $requestData, $this->headers);
        $this->assertBadRequest;
        $response->assertJson($this->responseJson(null, config('http_status.badRequest'), trans('message.passwordNotMatch')));
    }
}
