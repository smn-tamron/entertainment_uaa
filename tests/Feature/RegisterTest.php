<?php

namespace Tests\Feature;

use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->createdStatusCode = config('http_status.created');
        $this->reward = $this->createRewardType(config('enums.rewardType.firstRegister'));
        $this->user = $this->createUser(config('mock_data.emailLoginType'),config('mock_data.loginStatus.register'),config('enums.enableFirstStatus'), $this->reward->reward_coin);
        $this->getUserData =  $this->getUserData($this->user, config('mock_data.emailLoginType'));
    }

    public function getUserData($data, $loginType)
    {
        return [
            "type" => $loginType,
            "name" => $data->name,
            "emailOrPhone" => $loginType == config('mock_data.phoneLoginType') ? $data->phone : $data->email,
            "password" =>  config('mock_data.password'),
            "passwordConfirmation" => config('mock_data.password'),
            "deviceId" => config('mock_data.deviceId'),
            "deviceToken" => config('mock_data.deviceToken'),
            "deviceOsType" => config('mock_data.deviceOsType')
        ];
    }

    public function testRegisterWithEmailSuccessfully()
    {
        $response = $this->json('POST', 'api/v1.0/register/phone-or-email', $this->getUserData);
        $response->assertCreated();
        $response->assertJson(
            $this->successJson(
                [
                    config('mock_data.registerStatusKey') =>  config('mock_data.registerFalse')
                ],
                $this->createdStatusCode,
                trans('message.successOtpSend')
            )
        );
    }

    public function testRegisterWithPhoneSuccessfully()
    { 
        $user = $this->createUser(config('mock_data.phoneLoginType'),config('mock_data.loginStatus.register'),config('enums.enableFirstStatus'), $this->reward->reward_coin);
        $userData = $this->getUserData($user, config('mock_data.phoneLoginType'));

        $response = $this->json('POST', 'api/v1.0/register/phone-or-email', $userData);
        $response->assertCreated();
        $response->assertJson($this->successJson(
            [
                config('mock_data.registerStatusKey') =>  config('mock_data.registerFalse')
            ],
            $this->createdStatusCode,
            trans('message.successOtpSend')
        ));
    }

    public function testRegisterValidationFailed()
    {
        $userData = [
            "type" =>null,
            "name" => null,
            "emailOrPhone" => null,
            "password" =>  null,
            "passwordConfirmation" => null,
            "deviceId" => null,
            "deviceToken" => null,
            "deviceOsType" => null
        ];

        $response = $this->json('POST', 'api/v1.0/register/phone-or-email', $userData);
        $response->assertStatus($this->badRequestStatusCode);
        $response->assertJson($this->failedJsonForValidation());
    }

    public function testAlreadyRegistered()
    {
        $activeUser = $this->changeActiveStatusWhenOtpVerify($this->user->id);
        $requestData = [
            "type" => $activeUser->login_type,
            "name" => $activeUser->name,
            "emailOrPhone" => $activeUser->email,
            "password" =>  config('mock_data.password'),
            "passwordConfirmation" => config('mock_data.password'),
            "deviceId" => config('mock_data.deviceId'),
            "deviceToken" => config('mock_data.deviceToken'),
            "deviceOsType" => config('mock_data.deviceOsType')
        ];

        $response = $this->json('POST', 'api/v1.0/register/phone-or-email', $requestData);
        $response->assertOk();
        $response->assertJson(
            ($this->successJson(
                [
                    config('mock_data.registerStatusKey') =>  config('mock_data.registerTrue')
                ],
                $this->successStatusCode,
                trans('message.alreadyRegister')
            ))
        );
    }
}