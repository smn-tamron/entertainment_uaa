<?php

namespace Tests\Feature;

use Tests\TestCase;

class OtpTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->reward = $this->createRewardType(config('enums.rewardType.firstRegister'));
        $this->user = $this->createUser(config('mock_data.emailLoginType'),config('mock_data.loginStatus.register'),config('enums.enableFirstStatus'), $this->reward->reward_coin);
    }

    public function testSendOtpSuccessfully()
    {
        $requestData = [
           'email' => $this->user->email,
        ];
        $response = $this->json('POST', 'api/v1.0/send-otp', $requestData);
        $response->assertOk();
        $response->assertJson(
            $this->successJson(
                [
                    config('mock_data.sendOtpStatus') =>  config('mock_data.isSendTrue')
                ],
                $this->successStatusCode,
                trans('message.successOtpSend')
            )
        );
    }

    public function testSendOtpValidationFailed()
    {
        $requestData = [
           'email' => null
        ];
        $response = $this->json('POST', 'api/v1.0/send-otp', $requestData);
        $response->assertStatus($this->badRequestStatusCode);
        $response->assertJson($this->failedJsonForValidation());
    }
}
