<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Jenssegers\Mongodb\Collection;
use Jenssegers\Mongodb\Connection;
use Jenssegers\Mongodb\Eloquent\Model;

class ClientProfileTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->token = $this->createPassportToken($this->activeUser);
        $this->successMsg = trans('message.successMsg');
        $this->responseJson = $this->responseJson(null, $this->successStatusCode, $this->successMsg);
        $this->assertUnauthorized = $this->assertUnauthorized(config('http_status.unauthorized'));
        $this->headers = [ 'Accept' => 'application/json','Authorization' => "Bearer $this->token"];
    }
    /** @test */
    public function profileDetailsSuccess()
    {
        $response = $this->get("api/v1.0/profile/detail/".$this->activeUser->id, $this->headers);
        $response->assertOk();
        $response->assertJson(
            $this->responseJson(
                [
                    'user' => [
                        'id' => $this->activeUser->id,
                        'name' => $this->personalInfo->name,
                        'image' => getFileUrlFromAkoneyaMedia($this->personalInfo->image) ?? "",
                        'phone' => $this->personalInfo->phone,
                        'email' => $this->personalInfo->email,
                        'gender' => $this->personalInfo->gender,
                        'dateOfBirth' => $this->personalInfo->date_of_birth,
                        'bio' => $this->personalInfo->bio,
                        'address' => $this->personalInfo->address,
                        'loginType' => $this->activeUser->login_type
                    ]
                ],
                $this->successStatusCode,
                $this->successMsg
            )
        );
    }

    /** @test */
    public function uploadProfileImageSuccess()
    {
        $uploadImage = file_get_contents(public_path('mockdata/img/person.png'));
        $base64String =  base64_encode($uploadImage);
        $response = $this->json('POST', '/api/v1.0/profile/upload', [
            'id' => $this->personalInfo->client_user_id,
            'image' => $base64String
        ], $this->headers);
        //dd($response);
        $response->assertJson(
            $this->responseJson(
                [
                    "profileImage" => $response["result"]["profileImage"]
                ],
                $this->successStatusCode,
                $this->successMsg
            )
        );
    }

    /** @test */
    public function profileUpdateSuccess()
    {
        $response = $this->json('PUT', '/api/v1.0/profile/update', [
            'id' => $this->personalInfo->client_user_id,
            'name' => $this->personalInfo->name,
            'phone' => $this->personalInfo->phone,
            'gender' => config('mock_data.gender'),
            'email' => $this->personalInfo->email,
            'dateOfBirth' => $this->personalInfo->date_of_birth,
            'bio' => $this->personalInfo->bio,
            'address' => $this->personalInfo->address
        ], $this->headers);
        $response->assertOk();
        $response->assertJson($this->responseJson);
    }

    /** @test */
    public function profileDetailFailWhenUserIdInvalid()
    {
        $response = $this->get("api/v1.0/profile/detail/10", $this->headers);
        $this->assertUnauthorized;
        $response->assertJson($this->responseJson(null, config('http_status.unauthorized'), config('message.unauthorised')));
    }

    /** @test */
    public function uploadProfileImageValidationFail()
    {
        $response = $this->json('POST', '/api/v1.0/profile/upload', [
            'id' => null,
            'image' => null
        ], $this->headers);
        $this->assertBadRequest;
        $response->assertJson($this->responseJson(null, $this->badRequestStatusCode, config('message.profileUploadValidationFail')));
    }

    /** @test */
    public function profileUpdateValidationFail()
    {
        $response = $this->json('PUT', '/api/v1.0/profile/update', [
            'id' => $this->personalInfo->client_user_id,
            'name' => null,
            'phone' => $this->personalInfo->phone,
            'gender' => null,
            'email' => $this->personalInfo->email,
            'dateOfBirth' => $this->personalInfo->date_of_birth,
            'bio' => $this->personalInfo->bio,
            'address' => $this->personalInfo->address
        ], $this->headers);
        $this->assertBadRequest;
        $response->assertJson($this->responseJson(null, $this->badRequestStatusCode, config('message.profileUpdateValidationFail')));
    }
}
