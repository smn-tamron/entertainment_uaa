<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Carbon\Carbon;

class ForgetPasswordTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->successMsg = trans('message.successMsg');
        $this->user = $this->createUser(config('mock_data.emailLoginType'), "login", 1, 1000);
        $this->activeUser = $this->changeActiveStatusWhenOtpVerify($this->user->id);
    }

    public function getEncryptedOtpToken($userId)
    {
        $otpToken = $userId.".".Carbon::now()->timestamp;

        return  toEncryptToken($otpToken);
    }

    public function getOtpToken($emailOrPhone)
    {
        $user = User::where('email', $emailOrPhone)->orwhere('phone', $emailOrPhone)->first();

        $otpToken = $this->getEncryptedOtpToken($user->id);

        $user->update(['otp_token'=>$otpToken]);

        return $otpToken;
    }

    public function changeActiveStatusWhenOtpVerify($userId)
    {
        $user = User::find($userId);
        $user->active_status = 'active';
        $user->save();
        return $user;
    }

    /** @test */
    public function checkForForgetPasswordSuccessfully()
    {
        $userData = [
            "emailOrPhone" => $this->activeUser->email
        ];
        $response = $this->json('POST', 'api/v1.0/forget-password', $userData, ['Accept' => 'application/json']);
        $response->assertOk();
        $response->assertJson(
            $this->responseJson(
                [
                    config('mock_data.registerStatusKey') => config('mock_data.registerTrue')
                ],
                $this->successStatusCode,
                $this->successMsg
            )
        );
    }

    /** @test */
    public function checkForForgetPasswordFail()
    {
        $userData = [
            "emailOrPhone" =>  config('mock_data.unknownEmail')
        ];
        $response = $this->json('POST', 'api/v1.0/forget-password', $userData, ['Accept' => 'application/json']);
        $response->assertOk();
        $response->assertJson(
            $this->responseJson(
                [
                     config('mock_data.registerStatusKey') => config('mock_data.registerFalse')
                ],
                $this->successStatusCode,
                trans('message.accountExist')
            )
        );
    }

    /** @test */
    public function changePasswordSuccessfully()
    {
        $headers = [ 'Accept' => 'application/json','Otp-Token' => $this->getOtpToken($this->activeUser->email)];
        $response = $this->json('POST', 'api/v1.0/refresh-token', [], $headers);

        $userData = [
            "emailOrPhone" =>   $this->activeUser->email,
            "password" =>  config('mock_data.password'),
            "passwordConfirmation" =>  config('mock_data.password')
        ];
        $response = $this->json('POST', 'api/v1.0/new-password', $userData, $headers);
        $response->assertOk();
        $response->assertJson($this->responseJson(null, $this->successStatusCode, trans('message.successChangePassword')));
    }

    /** @test */
    public function changePasswordFail()
    {
        $headers = [ 'Accept' => 'application/json','Otp-Token' => $this->getOtpToken($this->activeUser->email)];
        $response = $this->json('POST', 'api/v1.0/refresh-token', [], $headers);

        $userData = [
            "emailOrPhone" =>   config('mock_data.unknownEmail'),
            "password" =>  config('mock_data.password'),
            "passwordConfirmation" =>  config('mock_data.password')
        ];
        $response = $this->json('POST', 'api/v1.0/new-password', $userData, $headers);
        $response->assertNotFound();
        $response->assertJson(
            $this->responseJson(
                null,
                $this->notFoundStatusCode,
                trans('message.notFoundMsg')
            )
        );
    }
}
