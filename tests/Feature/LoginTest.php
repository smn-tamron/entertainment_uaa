<?php

namespace Tests\Feature;

use Tests\TestCase;

class LoginTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }
   
    public function getUserDataForLogin($loginType, $loginStatus)
    {
        $this->reward = $this->createRewardType(config('enums.rewardType.firstRegister'));
        $this->user = $this->createUser($loginType,$loginStatus,config('enums.enableFirstStatus'), $this->reward->reward_coin);
        $this->activeUser = $this->changeActiveStatusWhenOtpVerify($this->user->id);
        $this->personalInfo = $this->createPersonalInfo($this->user);
        
        return [
            "deviceId" => config('mock_data.deviceId'),
            "deviceToken" => config('mock_data.deviceToken'),
            "deviceOsType" => config('mock_data.deviceOsType'),
            "loginType" => $loginType,
            "name" => (($loginType == config('mock_data.facebookLoginType')) || ($loginType == config('mock_data.googleLoginType')) || ($loginType == config('mock_data.appleLoginType'))) ? $this->personalInfo->name : null,
            "phone" => ($loginType == config('mock_data.phoneLoginType')) ? $this->personalInfo->phone : null,
            "image" => null,
            "email" => (($loginType == config('mock_data.emailLoginType')) || ($loginType == config('mock_data.googleLoginType')) || ($loginType == config('mock_data.appleLoginType'))) ? $this->personalInfo->email : null,
            "password" => (($loginType ==  config('mock_data.emailLoginType')) || ($loginType == config('mock_data.phoneLoginType'))) ? config('mock_data.password') : null,
            "facebookId" => $loginType == config('mock_data.facebookLoginType') ? $this->activeUser->facebook_id : null,
            "emailId" => $loginType == config('mock_data.googleLoginType') ? $this->activeUser->email_id : null,
            "appleId" => $loginType = config('mock_data.appleLoginType') ? $this->activeUser->apple_id : null,
        ];
    }

    public function prepareResponse($requestData)
    {
        $response = $this->json('POST', 'api/v1.0/login', $requestData);
        $response->assertOk();
        $responseResult = $response->decodeResponseJson();
        $this->assertIsInt($responseResult["result"]["user"]["id"]);
        $response->assertJson($this->successJson(
            [
                "user" => [
                        "id" => $responseResult["result"]["user"]["id"],
                        "name" => config('mock_data.name'),
                        "image" => getFileUrlFromAkoneyaMedia(config('mock_data.fakeProfile')),
                        "email" => $this->personalInfo->email,
                        "phone" => $this->personalInfo->phone,
                        "loginType" => $this->activeUser->login_type,
                        "initialUser" => true,
                        "rewardCoin"=> config('mock_data.rewardType.newbie')
                ],
                "accessToken" =>$responseResult["result"]["accessToken"]
            ],
            $this->successStatusCode,
            trans('message.successMsg')
        ));
    }

    public function testLoginWithPhoneSuccessfully()
    {
        $data = $this->getUserDataForLogin(config('mock_data.phoneLoginType'), 'login');
        $this->prepareResponse($data);
        $this->user->delete();
        $this->activeUser->delete();
        $this->personalInfo->delete();
    }

    public function testLoginWithEmailSuccessfully()
    {
        $data = $this->getUserDataForLogin(config('mock_data.emailLoginType'), 'login');
        $this->prepareResponse($data);
        $this->user->delete();
        $this->activeUser->delete();
        $this->personalInfo->delete();
    }

    public function testLoginWithFacebookSuccessfully()
    {
        $data = $this->getUserDataForLogin(config('mock_data.facebookLoginType'), 'login');
        $this->prepareResponse($data);
        $this->user->delete();
        $this->activeUser->delete();
        $this->personalInfo->delete();
    }

    public function testLoginWithGoogleSuccessfully()
    {
        $data = $this->getUserDataForLogin(config('mock_data.googleLoginType'), 'login');
        $this->prepareResponse($data);
        $this->user->delete();
        $this->activeUser->delete();
        $this->personalInfo->delete();
    }

    public function testLoginWithAppleSuccessfully()
    {
        $data = $this->getUserDataForLogin(config('mock_data.appleLoginType'), 'login');
        $this->prepareResponse($data);
        $this->user->delete();
        $this->activeUser->delete();
        $this->personalInfo->delete();
    }

    public function testLoginTypeValidationFailed()
    {
        $requestData = [
            "loginType" => null,
            "name" =>  null,
            "phone" =>  null,
            "image" => null,
            "email" => null,
            "password" => null,
            "facebookId" => null,
            "emailId" =>  null,
            "appleId" => null,
        ];
        $response = $this->json('POST', 'api/v1.0/login', $requestData);
        $response->assertStatus($this->badRequestStatusCode);
        $response->assertJson($this->failedJsonForValidation());
    }

    public function testIncorrectPhoneOrPassword()
    {
        $data = $this->getUserDataForLogin(config('mock_data.phoneLoginType'), 'login');
        $requestData = [
            "loginType" => $data['loginType'],
            "phone" => $data['phone'],
            "password" => config('mock_data.inCorrectPassword')
        ];
        $response = $this->json('POST', 'api/v1.0/login', $requestData);
        $response->assertOk();
        $response->assertJson($this->successJson(null, $this->successStatusCode, config('message.phoneLoginFail')));
    }

    public function testIncorrectEmailOrPassword()
    {
        $data = $this->getUserDataForLogin(config('mock_data.emailLoginType'), 'login');
        $requestData = [
            "loginType" => $data['loginType'],
            "email" => $data['email'],
            "password" => config('mock_data.inCorrectPassword')
        ];
        $response = $this->json('POST', 'api/v1.0/login', $requestData);
        $response->assertOk();
        $response->assertJson($this->successJson(null, $this->successStatusCode, config('message.EmailLoginFail')));
    }
}
