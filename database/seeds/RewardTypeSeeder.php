<?php

use Illuminate\Database\Seeder;

class RewardTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reward_types')->insert([
            [
                'game_category_id' => 2,
                'type' => 'guest',
                'reward_coin' => 1000
            ],
            [
                'game_category_id' => 2,
                'type' => 'newbie',
                'reward_coin' => 2000
            ]
        ]);
    }
}
