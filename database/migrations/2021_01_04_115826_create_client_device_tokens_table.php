<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientDeviceTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('client_device_tokens')) {
            Schema::create('client_device_tokens', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('client_user_id')->nullable();
                $table->string('device_id');
                $table->string('device_token')->nullable();
                $table->tinyInteger('device_os_type')->nullable();
                $table->timestamp('last_activated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_device_tokens');
    }
}
