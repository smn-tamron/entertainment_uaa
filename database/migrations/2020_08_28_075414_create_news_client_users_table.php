<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsClientUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone', 15)->nullable();
            $table->string('otp_code', 10)->nullable();
            $table->timestamp('otp_attempt_time')->nullable();
            $table->string('otp_token')->nullable();
            $table->string('active_status', 10)->nullable();
            $table->boolean('first_status')->default(1);
            $table->string('login_type', 20)->nullable();
            $table->string('user_type', 20)->nullable();
            $table->integer('wrong_time')->nullable();
            $table->text('facebook_id')->nullable();
            $table->text('email_id')->nullable();
            $table->text('apple_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 225)->nullable();
            $table->longText('remember_token')->nullable();
            $table->integer('coin')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_users');
    }
}
