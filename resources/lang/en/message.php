<?php

return [
    
    'alreadyRegister' => '!This email or phone number is already registered.',
    'successRegister' => 'Your account is succefully registered.',
    'successOtpSend' => 'Otp code is successfully sent.',
    'mailSubject' => 'Akoneya',
    'mailBody' => 'Your otp code is ',
    'otpSuccess' => 'Verified successfully.',
    'invalidOtp' => 'Invalid your otp.',
    'successMsg' => 'Success',
    'deleteSuccessMsg' => 'Deleted successfully.',
    'notFoundMsg' => 'No record.',
    'currentPasswordFail' => '!Please enter a valid password.',
    'successChangePassword' => 'Password successfully changed.',
    'otpExpired' => 'Your otp is expire.Try again.',
    'phoneLoginFail' => 'Incorrect phone or password.',
    'EmailLoginFail' => 'Incorrect email or password.',
    'accountExist' => 'This account does not exist.',
    'successLogout' => 'Logout successfully.',
    'unauthorised'=>'Unauthorized',
    'tokenInvalidOrExpire'=>'!The token is either invalid or expired.',
    'passwordNotMatch'=>'!The passwords you entered do not match.',
    'invalidEmail' => 'Your email is invalid',
    'invalidLoginType' => 'Invalid loginType.',
    'invalidType' => 'Invalid Type. ',
    'imageSizeLimitation' => 'File size must be less than 600 KB.',
    'otpTokenInvalidOrExpire'=>'!The otp token is either invalid or expired.',
    'hadPhNo' => 'Had phone number.',
    'requiredPassword' => 'Required password.',
    'phoneAlreadyInUse' => 'This phone number is already in use for another account.',
    'alreadyRead' => 'This notification was already read.'
];
