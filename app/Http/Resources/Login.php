<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Login extends JsonResource
{
    private $reward;

    public function __construct($resource, $reward)
    {   
        $this->reward = $reward;
    
        parent::__construct($resource);
    }
    
    public function toArray($request)
    {
        return[
                'id' => $this->id,
                'name' => $this->name,
                'image' => getFileUrlFromAkoneyaMedia($this->image) ?? "",
                'email' => $this->email ?? "",
                "phone" => $this->phone ?? "",
                "loginType" => $this->login_type,
                "initialUser" => $this->first_status? true : false,
                "rewardCoin" => $this->first_status? $this->reward : 0
            ];
    }
}
