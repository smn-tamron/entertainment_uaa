<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserInfo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
                'id' => $this->client_user_id,
                'name' => $this->name,
                'image' => getFileUrlFromAkoneyaMedia($this->image) ?? "",
            ];
    }
}
