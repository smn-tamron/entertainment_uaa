<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AppVersionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'versionName' => $this->version_name,
            'deviceOsType' => $this->os_type,
            'isForceUpdate' => (boolean)$this->is_force_update,
            'playStoreUrl' => $this->play_store_url,
            'appStoreUrl' => $this->app_store_url,
            'directDownloadUrl' => $this->direct_download_url
        ];
    }
}
