<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GuestUser extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
                'id' => $this->id,
                'name' => $this->name,
                'image' => getFileUrlFromAkoneyaMedia($this->image) ?? "",
                "userType" => $this->user_type,
                "initialUser" => $this->first_status? true : false,
                "rewardCoin" => $this->first_status? $this->coin : 0
            ];
    }
}
