<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Exception;

class OtpTokenValidationMiddlware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   

        $otpToken = $request->header('Otp-Token');

        if(!$otpToken) {
            return response()->json([
                'result' => null,
                'statusCode' => 400,
                'message' => trans('message.otpTokenInvalidOrExpire')
            ], 400);
        } 

        if($otpToken){

            try {
                $decryptedToken  = decryptToken($otpToken);
                $accessToken = explode(".", $decryptedToken);
    
                $otpUser = $accessToken[0];
            
                $startTime = Carbon::createFromTimeStamp($accessToken[1]);
    
                $now = Carbon::now('UTC');
            
                $totalDuration = $startTime->diffInMinutes($now); 
        
                if ($totalDuration >= config('services.otpTokenExpire')) {
    
                    return response()->json([
                        'result' => null,
                        'statusCode' => 400,
                        'message' => trans('message.otpTokenInvalidOrExpire')
                    ]);
                }

            }
            catch (\Exception $e) {
                    return response()->json([
                        'result' => null,
                        'statusCode' => 400,
                        'message' => trans('message.otpTokenInvalidOrExpire')
                    ], 400);
            }

            $request->merge(["otpUser" => $otpUser ]);

           
        } 


        return $next($request);
    }
        
    
}
