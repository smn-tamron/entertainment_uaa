<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Log;

class RequestLogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $requestLog['method'] = $request->method();
        $requestLog['path'] = $request->path();
        $requestLog['payload'] = $this->payload($request);
        $requestLog['response'] =  $response->getContent();

        Log::info(json_encode($requestLog));

        return $response;
    }

    protected function payload($request)
    {
        $allFields = $request->all();

        foreach (config('logging.api.except', []) as $key) {
            if (array_key_exists($key, $allFields)) {
                unset($allFields[$key]);
            }
        }

        return json_encode($allFields);
    }

}
