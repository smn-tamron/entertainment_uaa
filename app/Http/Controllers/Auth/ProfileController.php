<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ResponseRepository;
use App\Repositories\ProfileRespository;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Profile as ProfileResource;
use App\Http\Resources\UserInfo as UserInfoResource;
use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Requests\ProfileUploadRequest;

class ProfileController extends Controller
{
    public function __construct(ResponseRepository $responseRepo, ProfileRespository $profileRepo)
    {
        $this->responseRepo = $responseRepo;
        $this->profileRepo = $profileRepo;

        $this->successStatusCode = config('http_status.success');
        $this->badRequestStatusCode = config('http_status.badRequest');
        $this->notFoundStatusCode = config('http_status.notFound');
        $this->unauthorizedStatusCode = config('http_status.unauthorized');
        $this->unauthorizedMsg = trans('message.unauthorised');
        $this->notFoundMsg = trans('message.notFoundMsg');
        $this->successMsg =  trans('message.successMsg');
    }

    public function detail(Request $request, $id)
    {
        $data = null;
        
        if (!check_auth_user_id($request->auth_user_id, $id)) {
            return $this->responseRepo->failResponse(null, $this->unauthorizedStatusCode, $this->unauthorizedMsg);
        }
        $userData = $this->profileRepo->getById($id);
        $clientPersonalInfo = $this->profileRepo->clientPersonalInfo($userData->id);
        if ($clientPersonalInfo) {
            $data = new ProfileResource($clientPersonalInfo);
            return $this->responseRepo->successResponse(['user'=> $data], $this->successStatusCode, $this->successMsg);
        }
        return $this->responseRepo->failResponse(null, $this->notFoundStatusCode, $this->notFoundMsg);
    }
    
    public function upload(ProfileUploadRequest $request)
    {
        $data = null;
        if (!check_auth_user_id($request->auth_user_id, $request->id)) {
            return $this->responseRepo->failResponse($data, $this->unauthorizedStatusCode, $this->unauthorizedMsg);
        }
            
        $userData = $this->profileRepo->getById($request->id);
        
        $data["profileImage"] = $this->profileRepo->updatePersonalProfileImage($userData->id, $request->image);
        return $this->responseRepo->successResponse($data, $this->successStatusCode, $this->successMsg);
    }

    public function update(ProfileUpdateRequest $request)
    {
        $data = null;
        if (!check_auth_user_id($request->auth_user_id, $request->id)) {
            return $this->responseRepo->failResponse($data, $this->unauthorizedStatusCode, $this->unauthorizedMsg);
        } else {
            $userData = $this->profileRepo->getById($request->id);
            $this->profileRepo->updateClientPersonalInfo($userData->id, $userData->login_type, $request);
            return $this->responseRepo->successResponse($data, $this->successStatusCode, $this->successMsg);
        }
    }

    public function getAdminProfile(Request $request)
    {
        $data = null;
        $adminInfo = $this->profileRepo->adminUserInfo();
        if ($adminInfo) {
            $personalInfo = $this->profileRepo->clientPersonalInfo($adminInfo->id);
            return $this->responseRepo->successResponse(['admin'=> new UserInfoResource($personalInfo)], $this->successStatusCode, $this->successMsg);
        }
        return $this->responseRepo->failResponse(null, $this->notFoundStatusCode, $this->notFoundMsg);
    }
}
