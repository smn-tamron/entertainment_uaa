<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ResponseRepository;
use App\Repositories\AuthUserRepository;
use App\Http\Requests\DeviceTokenRequest;

class AuthController extends Controller
{
    public function __construct(ResponseRepository $responseRepo, AuthUserRepository $userRepo)
    {
        $this->responseRepo = $responseRepo;
        $this->successStatusCode = config('http_status.success');
        $this->badRequestStatusCode = config('http_status.badRequest');
        $this->successMsg = config('message.successMsg');
        $this->userRepo = $userRepo;
    }

    public function getUser(){
        return $this->userRepo->getUser();
    }

    public function getPersonalInfo($id){
        return $this->userRepo->getPersonalInfo($id);
    }

    public function getDeviceToken(DeviceTokenRequest $request)
    {
        $result = $this->userRepo->upsertDeviceToken($request->auth_user_id,$request->deviceId,$request->deviceToken,$request->deviceOsType);
        return $this->responseRepo->successResponse($result, $this->successStatusCode, $this->successMsg);
    }
}