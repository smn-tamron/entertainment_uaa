<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ResponseRepository;
use App\Repositories\OtpRepository;

class OtpController extends Controller
{
    public function __construct(ResponseRepository $responseRepo, OtpRepository $otpRepo)
    {
        $this->responseRepo = $responseRepo;
        $this->otpRepo = $otpRepo;
        $this->badRequestStatusCode = config('http_status.badRequest');
    }

    public function sendOtp(Request $request)
    {
        $require = 'required';

        $validator = validator(request()->all(), [
            'email'=> $require,
            ]);
        if ($validator->fails()) {
            $error_text="";

            foreach ($validator->errors()->all() as $error) {
                $error_text .= $error;
            }
            return $this->responseRepo->failResponse(null, $this->badRequestStatusCode, $error_text);
        }
        return $this->otpRepo->sendOtp($request->email);
    }

    public function otpVerify(Request $request)
    {
        $require = 'required';

        $validator = validator(request()->all(), [
            'emailOrPhone'=> $require,
            'otpCode'=> $require,
            ]);
        if ($validator->fails()) {
            $error_text="";

            foreach ($validator->errors()->all() as $error) {
                $error_text .= $error;
            }
            return $this->responseRepo->failResponse(null, $this->badRequestStatusCode, $error_text);
        }
        return $this->otpRepo->verifyOtp(request()->all());
    }
}
