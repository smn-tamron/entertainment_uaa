<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ResponseRepository;
use App\Repositories\RegisterRepository;
use App\Repositories\OtpRepository;
use App\Repositories\ForgetPasswordRepository;

class ForgetPasswordController extends Controller
{
    public function __construct(ResponseRepository $responseRepo, RegisterRepository $regRepo, OtpRepository $otpRepo, ForgetPasswordRepository $ForgetPasswordRepository)
    {
        $this->responseRepo = $responseRepo;
        $this->regRepo = $regRepo;
        $this->otpRepo = $otpRepo;
        $this->ForgetPasswordRepository = $ForgetPasswordRepository;
       
        $this->successStatusCode = config('http_status.success');
        $this->badRequestStatusCode = config('http_status.badRequest');
        $this->notFoundStatusCode = config('http_status.notFound');
        $this->notFoundMsg = config('message.notFoundMsg');
        $this->createNewPassword = config('message.createNewPassword');
    }
   
    public function forgetPassword(Request $request)
    {
        $validator = validator(request()->all(), [
            'emailOrPhone' => 'required'
        ]);

        if ($validator->fails()) {
            $error_text="";
            foreach ($validator->errors()->all() as $error) {
                $error_text .= $error;
            }
            return $this->responseRepo->failResponse(null, $this->badRequestStatusCode, $error_text);
        }

        return $this->ForgetPasswordRepository->forgetPassword($request->all());
    }

    public function createNewPassword(Request $request)
    {
        $validator = validator(request()->all(), [
            'emailOrPhone' => 'required',
            'password' => 'required|min:6',
            'passwordConfirmation' => 'required|min:6'
        ]);
        if($request->password != $request->passwordConfirmation){
            return $this->responseRepo->failResponse(null, $this->badRequestStatusCode, trans('message.passwordNotMatch'));
        }
        if ($validator->fails()) {
            $error_text="";

            foreach ($validator->errors()->all() as $error) {
                $error_text .= $error;
            }

            return $this->responseRepo->failResponse(null, $this->badRequestStatusCode, $error_text);
        }
        return $this->ForgetPasswordRepository->createNewPassword($request->all());
    }
}
