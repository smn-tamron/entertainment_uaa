<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ResponseRepository;
use App\Repositories\RegisterRepository;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\DeviceTokenRequest;

class RegisterController extends Controller
{
    public function __construct(ResponseRepository $responseRepo, RegisterRepository $registerRepo)
    {
        $this->responseRepo = $responseRepo;
        $this->registerRepo = $registerRepo;
        $this->successMsg = trans('message.successMsg');
        $this->successStatusCode = config('http_status.success');
        $this->badRequestStatusCode = config('http_status.badRequest');
    }

    public function createGuestUser(DeviceTokenRequest $request)
    {
        return $this->registerRepo->createGuestUser(request()->all());
    }

    public function register(RegisterRequest $request)
    {
        $checkRegisterType = (in_array($request->type, [config('login_types.phone'),config('login_types.email')])) ? 'TRUE' : 'FALSE';

        if ($checkRegisterType == 'FALSE') {
            return $this->responseRepo->failResponse(null, $this->badRequestStatusCode, trans('message.invalidType'));
        }

        return $this->registerRepo->register(request()->all());
    }

    public function checkRegister(Request $request)
    {
        validator(request()->all(), [
            'emailOrPhone'=> 'required',
        ]);
        $checkUser = $this->registerRepo->checkRegister($request->emailOrPhone);
        if (empty($checkUser)) {
            $data = ['isRegistered'=>false];

            return $this->responseRepo->successResponse($data, $this->successStatusCode, trans('message.notFoundMsg'));
        }
        $data = ['isRegistered'=>true];

        return $this->responseRepo->successResponse($data, $this->successStatusCode, trans('message.alreadyRegister'));
    }
}
