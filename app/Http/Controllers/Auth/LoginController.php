<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ResponseRepository;
use App\Repositories\LoginRepository;
use App\Http\Requests\PartnerLoginRequest;
use App\Http\Requests\PhoneNoRequest;

class LoginController extends Controller
{
    public function __construct(ResponseRepository $responseRepo, LoginRepository $loginRepo)
    {
        $this->responseRepo = $responseRepo;
        $this->successStatusCode = config('http_status.success');
        $this->badRequestStatusCode = config('http_status.badRequest');
        $this->successMsg = config('message.successMsg');
        $this->loginRepo = $loginRepo;
    }

    public function login(Request $request)
    {
        $loginType = 'loginType';
        $require = 'required';
        $requireStr = 'required|string';
        $email = 'email';

        $checkLoginType = (in_array($request->loginType, array_values(config('login_types')))) ? 'TRUE' : 'FALSE';

        if($checkLoginType == 'FALSE' ) {
            return $this->responseRepo->failResponse(null, $this->badRequestStatusCode,trans('message.invalidLoginType'));
        }

        if ($request->loginType == config('login_types.email')) {
            $validator = validator(request()->all(), [
                $loginType =>$require,
                $email => $requireStr,
                'password' => $requireStr

            ]);
        } elseif ($request->loginType == config('login_types.phone')) {
            $validator = validator(request()->all(), [
                $loginType =>$require,
                'phone' => $requireStr,
                'password' => $requireStr

            ]);
        } elseif ($request->loginType == config('login_types.facebook')) {
            $validator = validator(request()->all(), [
                $loginType =>$require,
                'name' => $require,
                'facebookId'=> $require

            ]);
        } elseif ($request->loginType == config('login_types.google')) {
            $validator =  validator(request()->all(), [
                $loginType =>$require,
                'name' => $require,
                $email=> $require,
                'emailId'=> $require
            ]);
        } else { // apple login
            $validator =  validator(request()->all(), [
                $loginType =>$require,
                'name' => $require,
                $email=> $require,
                'appleId'=> $require
            ]);
        }
       
        if ($validator->fails()) {
            $error_text="";

            foreach ($validator->errors()->all() as $error) {
                $error_text .= $error;
            }

            return $this->responseRepo->failResponse(null, $this->badRequestStatusCode, $error_text);
        }
        
        if ($request->loginType == config('login_types.phone') || $request->loginType == config('login_types.email')) {
            return $this->loginRepo->loginWithPhoneOrEmail($request);
        } else {
            return $this->loginRepo->socialLogin($request);
        }
    }

}
