<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ChangePasswordRepository;

class ChangePasswordController extends Controller
{
    public function __construct(ChangePasswordRepository $changeRepo)
    {
        $this->changeRepo = $changeRepo;
    }
    
    public function changePassword(ChangePasswordRequest $request)
    {
        return $this->changeRepo->changePassword($request->all(), $request->auth_user_id);
    }
}
