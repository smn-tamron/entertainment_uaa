<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ResponseRepository;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repositories\AuthUserRepository;

class RefreshTokenController extends Controller
{   
    public function __construct(ResponseRepository $responseRepo,AuthUserRepository $authUserRepo)
    {
        $this->ResponseRepository = $responseRepo;
        $this->unauthorizedStatusCode = config('http_status.unauthorized');
        $this->successStatusCode = config('http_status.success');
        $this->successLogoutMsg = config('message.successLogout');
        $this->accessToken = null;
        $this->refreshTokenInterval = config('jwt.refresh_before_exp');
        $this->authUserRepo = $authUserRepo;

    }
    public function getExpFromToken($token)
    {
        $encodedStr = explode('.', $token);
        $str = $encodedStr[1];
        $decodedStr =  base64_decode($str);
        $getArray = json_decode($decodedStr, true);
        return (strtotime(date('Y-m-d H:i:s', $getArray['exp'])) - strtotime(date("Y-m-d H:i:s")))/60;
    }

    public function refresh(Request $request)
    {
        try
        {
            $diffInMinutes = $this->getExpFromToken(JWTAuth::getToken());

            if($diffInMinutes <= $this->refreshTokenInterval){
                JWTAuth::parseToken()->authenticate();
                $this->accessToken = JWTAuth::refresh(JWTAuth::getToken());
                $this->authUserRepo->upsertDeviceToken(auth()->user()->id,$request->deviceId,$request->deviceToken,$request->deviceOsType);
            }
            else{
                $this->accessToken = JWTAuth::getToken();
            }

        }catch (JWTException $e){
            
            return response()->json([
                'result'=> null,
                'statusCode'=> 40103,
                'message' => 'Token cannot be refreshed.'
            ],401);
        }

        $accessToken  = toEncryptToken($this->accessToken);
        $data = ['accessToken'=>$accessToken];

        return $this->ResponseRepository->successResponse($data, $this->successStatusCode, trans('message.successMsg'));
    }
}
