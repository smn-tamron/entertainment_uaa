<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\AuthUserRepository;
use App\Repositories\ResponseRepository;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class LogoutController extends Controller
{
    public function __construct(ResponseRepository $responseRepo, AuthUserRepository $authUserRepo)
    {
        $this->ResponseRepository = $responseRepo;
        $this->authUserRepo = $authUserRepo;
        $this->unauthorizedStatusCode = config('http_status.unauthorized');
        $this->successStatusCode = config('http_status.success');
        $this->successLogoutMsg = config('message.successLogout');
        $this->unauthorisedMsg = config('message.unauthorised');
        $this->notFoundStatusCode = config('http_status.badRequest');
    }

    public function logout(Request $request)
    {   
        if (Auth::check()) {
            $token = $request->header( 'Authorization' );
            JWTAuth::parseToken()->invalidate($token);
            return $this->ResponseRepository->successResponse(null, $this->successStatusCode, trans('message.successLogout'));
        } else{
            return $this->ResponseRepository->failResponse(null, $this->notFoundStatusCode, trans('message.notFoundMsg'));
        }       
    }

}
