<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AppVersionCheckRequest;
use App\Repositories\AppVersionRepository;
use App\Repositories\ResponseRepository;
use App\Http\Resources\AppVersionResource;

class AppVersionController extends Controller
{
    public function __construct(AppVersionRepository $appVersionRepo,ResponseRepository $responseRepo)
    {
        $this->appVersionRepo = $appVersionRepo;
        $this->responseRepo = $responseRepo;
    }

    public function checkAppVersion(AppVersionCheckRequest $request)
    {
    	$appVersion = $this->appVersionRepo->checkByVersionNameAndOsType($request->versionName,$request->deviceOsType);
    	if(!$appVersion) {
    		return $this->responseRepo->failResponse(null, $this->responseRepo->badRequestStatusCode,$this->responseRepo->notFoundMsg);
    	}

    	return $this->responseRepo->successResponse(new AppVersionResource($appVersion),$this->responseRepo->successStatusCode,$this->responseRepo->successMsg);
    }
}
