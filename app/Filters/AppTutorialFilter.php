<?php

namespace App\Filters;

use Illuminate\Support\Facades\DB;
use App\Filters\BaseFilter;

class AppTutorialFilter extends BaseFilter
{
    protected $filters = ['keyWord'];
     
    public function keyWord($value)
    {
        return $this->builder->where('title','LIKE','%' . $value . '%' )->orwhere('title_mm','LIKE','%' . $value . '%')->orwhere('title_zh','LIKE','%' . $value . '%');
    }
}