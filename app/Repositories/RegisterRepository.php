<?php
namespace App\Repositories;

use JWTAuth;
use App\User;
use App\Repositories\UserRewardRepository;
use App\Repositories\ResponseRepository;
use App\Services\MailService;
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\GuestUser as GuestUserResource;

class RegisterRepository extends BaseRepository
{
    public function __construct(User $userModel, UserRewardRepository $userRewardRepo, ResponseRepository $responseRepo, BaseRepository $baseRepo, MailService $mailService, AuthUserRepository $authUserRepo)
    {
        $this->userModel = $userModel;
        $this->authUserRepo =$authUserRepo;
        $this->responseRepo =$responseRepo;
        $this->userRewardRepo = $userRewardRepo;
        $this->baseRepo = $baseRepo;
        $this->mailService = $mailService;
        $this->disableFirstStatus = config('enums.disableFirstStatus');
        $this->successMsg = trans('message.successMsg');
        $this->successStatusCode = config('http_status.success');
        $this->mailSubject = config('message.mailSubject');
        $this->mailBody = config('message.mailBody');
        $this->guest = config('enums.guestUser');
        $this->disableBannedStatus = config('enums.disableBannedStatus');
        $this->guestUserStatus = config('enums.userStatus.guest');
    }

    public function createGuestUser($data)
    {
        $device = $this->authUserRepo->checkDeviceByUserStatus($data, $this->guestUserStatus);
       
        if ($device){
            $existingUser =  $this->userModel->where('id', $device->client_user_id)
                                                ->where('user_type', $this->guest)
                                                    ->where('banned_status', $this->disableBannedStatus)->first();
        
            $this->baseRepo->updateFirstStatusForInitUser($existingUser);
            $token = JWTAuth::fromUser($existingUser);
            $guestUser = $this->getGuestUserInfo($existingUser->id);
        } else {
            $reward = $this->userRewardRepo->getFirstTimeReward($this->guest);
            $newUser = $this->baseRepo->toRegisterAsGuest($reward->reward_coin);
            $this->baseRepo->cloneToProfile($newUser, $newUser->id);
            $this->authUserRepo->upsertDeviceToken($newUser->id, $data['deviceId'], $data['deviceToken'], $data['deviceOsType'], $this->guestUserStatus);
            $token = JWTAuth::fromUser($newUser);
            $guestUser = $this->getGuestUserInfo($newUser->id);
        }
      
        $accessToken = toEncryptToken($token);
        return $this->responseRepo->successResponse(['user' => $guestUser, 'accessToken' => $accessToken], $this->successStatusCode, $this->successMsg);
    }

    public function getGuestUserInfo($userId)
    {
        $clientUserId = 'client_users.id';
        $userInfo = User::join('personal_information', 'personal_information.client_user_id', '=', $clientUserId)
                            ->select(
                                'client_users.id',
                                'personal_information.name',
                                'personal_information.image',
                                'client_users.user_type',
                                'client_users.first_status', 
                                'client_users.coin'
                            )
                            ->where($clientUserId, $userId)
                            ->first();
        return new GuestUserResource($userInfo);
    }

    public function register($data)
    {
        $statusCode = $this->responseRepo->createStatusCode;
        $message = trans('message.successOtpSend');
        $checkuser = $this->checkRegister($data);
        $data['is_forget'] = "false";

        if ($checkuser) {
            return $this->responseRepo->successResponse([$this->baseRepo->isRegistered => true], $this->responseRepo->successStatusCode, trans('message.alreadyRegister'));
        }

        if ($data['type'] == $this->baseRepo->phoneField) {
            $this->userModel::where($this->baseRepo->phoneField, $data[$this->baseRepo->emailOrPhoneField])->where($this->baseRepo->activeStatusField, $this->baseRepo->pendingActiveStatus)->delete();
           
            $this->baseRepo->toTempRegister($data);
        } else {
            $isValidEmail = $this->baseRepo->checkEmail($data[$this->baseRepo->emailOrPhoneField]);

            if (!$isValidEmail) {
                $statusCode = $this->responseRepo->badRequestStatusCode;
                $message = trans('message.invalidEmail');
            } else {
                $this->userModel::where($this->baseRepo->emailField, $data[$this->baseRepo->emailOrPhoneField])->where($this->baseRepo->activeStatusField, $this->baseRepo->pendingActiveStatus)->delete();
                $data['emailOrPhone'] = $data[$this->baseRepo->emailOrPhoneField];
                $data['otp_code'] =  $this->baseRepo->generateNumericOTP(6);
                $data['otp_attempt_time'] = date('Y-m-d H:i:s');
                $userData = $this->baseRepo->toTempRegister($data);
                $this->mailService->phpMailer($data['emailOrPhone'], $this->mailSubject, $this->mailBody.$data['otp_code']);
              
                return $this->responseRepo->successResponse([$this->baseRepo->isRegistered => false], $this->responseRepo->createStatusCode, $message);
            }
        }
        return $this->responseRepo->failResponse([$this->baseRepo->isRegistered => false], $statusCode, $message);
    }

    public function checkRegister($data)
    {
        if ($data['type'] == config('login_types.email')) {
            return $this->userModel::where($this->baseRepo->activeStatusField, "=", $this->baseRepo->enableActiveStatus)
                                ->where($this->baseRepo->emailField, $data[$this->baseRepo->emailOrPhoneField])
                                ->where('banned_status', $this->disableBannedStatus)->first();
        } else {
            return $this->userModel::where($this->baseRepo->activeStatusField, "=", $this->baseRepo->enableActiveStatus)
                                ->where($this->baseRepo->phoneField, $data[$this->baseRepo->emailOrPhoneField])
                                ->where('banned_status', $this->disableBannedStatus)->first();
        }
    }
}
