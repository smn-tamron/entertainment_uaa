<?php
namespace App\Repositories;

use App\User;
use App\PersonalInformation;
use App\Repositories\UserRewardRepository;
use App\Repositories\ResponseRepository;
use App\Services\MailService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class OtpRepository extends BaseRepository
{
    public function __construct(User $userModel, UserRewardRepository $userRewardRepo,ResponseRepository $responseRepo, BaseRepository $baseRepo, MailService $mailService,AuthUserRepository $authUserRepo)
    {
        $this->responseRepo =$responseRepo;
        $this->baseRepo = $baseRepo;
        $this->userModel = $userModel;
        $this->mailService = $mailService;
        $this->authUserRepo = $authUserRepo;
        $this->userRewardRepo = $userRewardRepo;
        $this->successStatusCode = config('http_status.success');
        $this->notFoundStatusCode = config('http_status.notFound');
        $this->badRequestStatusCode = config('http_status.badRequest');

        $this->placeholder_image_url = config('mock_data.fakeProfile');
        $this->mailSubject = config('message.mailSubject');
        $this->mailBody = config('message.mailBody');

        $this->emailType = config('login_types.email');
        $this->phoneField = config('login_types.phone');
        $this->otpTokenExpire = config('services.otpTokenExpire');
        $this->registeredUserStatus = config('enums.userStatus.registered');
        $this->guestUserStatus = config('enums.userStatus.guest');
    }

    public function sendOtp($email)
    {
        $user = $this->userModel::where($this->baseRepo->emailField, $email)
                                    ->orderby('id', 'desc')
                                    ->first();
        if (empty($user)) {
            return $this->responseRepo->failResponse([$this->baseRepo->otpSend => $this->baseRepo->otpSendFail], $this->successStatusCode, trans('message.notFoundMsg'));
        }
        $user['otp_code'] = $this->baseRepo->generateNumericOTP(6);
        $user['otp_attempt_time'] = date('Y-m-d H:i:s');
        $user->save();

        $data['user_id'] = $user->id;
        $data['type'] =  $user->login_type;
        $data['is_forget'] = "true";
        $data['emailOrPhone'] = $user->email;
        $data['otp_code'] = $user->otp_code;
        $data['otp_attempt_time'] = $user->otp_attempt_time;

        $this->baseRepo->toTempRegister($data);

        $this->mailService->phpMailer($user->email, $this->mailSubject, $this->mailBody.$user->otp_code);

        return $this->responseRepo->successResponse([$this->baseRepo->otpSend => $this->baseRepo->otpSendSuccess], $this->successStatusCode, trans('message.successOtpSend'));
    }

    public function verifyOtp($data)
    {
        $statusCode = $this->successStatusCode;
        $message = trans('message.invalidOtp');
        $otpStatus = $this->baseRepo->otpVerifyFail;
        $isValidEmail = $this->baseRepo->checkEmail($data[$this->baseRepo->emailOrPhoneField]);
       
        if ($isValidEmail) {
            $user = json_decode(Cache::store('redis')->get('client_users.'.$data[$this->baseRepo->emailOrPhoneField]), true);
        } else {
            $user =  json_decode(Cache::store('redis')->get('client_users.'.$data[$this->baseRepo->emailOrPhoneField]), true);
        }
        if (!$user) {
            $message = trans('message.notFoundMsg');
        }
        else {
         
            if ($user['type'] == $this->emailType && ($user['otp_code'] == $data['otpCode'])) {
                $diffInMinutes=(strtotime(date("Y-m-d H:i:s"))-strtotime($user['otp_attempt_time']))/60;
               
                if ($diffInMinutes > config('services.otpExpire')) {
                    $message = trans('message.otpExpired');
                    return $this->responseRepo->failResponse([$this->baseRepo->otpVerified =>$otpStatus,'otpToken'=>null], $statusCode, $message);
                } else {
                    $otpStatus = $this->baseRepo->otpVerifySuccess;
                    $message = trans('message.otpSuccess');
                }
            } elseif ($user['type'] == $this->phoneField) {
                $otpStatus = $this->baseRepo->otpVerifySuccess;
                $message = trans('message.otpSuccess');
            }
            if ($otpStatus == false) {
                return $this->responseRepo->failResponse([$this->baseRepo->otpVerified =>$otpStatus,'otpToken'=>null], $statusCode, $message);
            }

            if($user['is_forget'] == "false"){

                $oldUser = $this->findUser($user['type'],$user['emailOrPhone']);
        
                if($oldUser == null){

                    $reward = $this->userRewardRepo->getFirstTimeReward(config('enums.rewardType.firstRegister'));
                    $existingDevice = $this->authUserRepo->checkDeviceByUserStatus($user['deviceId'],$this->guestUserStatus);

                    if ($existingDevice){
                        $guestUser = $this->userModel->where('id', $existingDevice->client_user_id)->first();
                        $user['coin'] = $reward->reward_coin + $guestUser->coin;
                    }else{
                        $user['coin'] = $reward->reward_coin;
                    }
             
                    $newUser = $this->baseRepo->toRegister($user);

                    $newUser['type'] = $user['type'];
                    $this->baseRepo->cloneToProfile($newUser, $newUser->id);
                    $userId = $newUser->id;

                } else{
                    $userId = $oldUser->id;
                }
                
                $this->authUserRepo->upsertDeviceToken($userId, $user['deviceId'],$user['deviceToken'],$user['deviceOsType'], $this->registeredUserStatus);

            } else{
                $userId = $user['user_id'];
            }

            $encryptedToken = $this->getEncryptedOtpToken($userId);
           
            Cache::store('redis')->forget('client_users.'.$data[$this->baseRepo->emailOrPhoneField]);

            return $this->responseRepo->successResponse([$this->baseRepo->otpVerified => $this->baseRepo->otpVerifySuccess, 'otpToken'=> $encryptedToken], $statusCode, $message);
        }

        return $this->responseRepo->failResponse([$this->baseRepo->otpVerified =>$otpStatus,'otpToken'=>null], $statusCode, $message);
    }

    public function getEncryptedOtpToken($userId)
    {
        $otpToken = $userId.".".Carbon::now()->timestamp;

        $encryptedToken = toEncryptToken($otpToken);

        $this->userModel::where('id', $userId)->update(["otp_token"=>$encryptedToken]);

        return $encryptedToken;
    }

    public function findUser($field, $emailOrFail)
    {
        return $this->userModel::where($field, $emailOrFail)->where('active_status','active')
                                    ->first();
    }
}
