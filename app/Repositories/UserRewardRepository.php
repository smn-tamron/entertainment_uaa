<?php
namespace App\Repositories;

use App\User;
use App\Exceptions\NotFoundException;
use App\RewardType;

class UserRewardRepository extends BaseRepository
{
    public function __construct(RewardType $rewardTypeModel,User $userModel)
    {
        $this->userModel =$userModel;
        $this->rewardTypeModel = $rewardTypeModel;
        $this->firstRegister = config('enums.rewardType.firstRegister');
        $this->firstTryGuest = config('enums.rewardType.guest');
        $this->slotGame = config('enums.gameCategory.slotReel');
        $this->guest = config('enums.guestUser');
        $this->disableFirstStatus = config('enums.disableFirstStatus');
    }

    public function getFirstTimeReward($userType)
    {
        if ($userType != $this->guest){
            return $this->getSlotGameReward($this->firstRegister);
        } else {
            return $this->getSlotGameReward($this->firstTryGuest);
        }
    }

    public function getSlotGameReward($type)
    {
        return $this->rewardTypeModel->select('type','reward_coin')->where('game_category_id', $this->slotGame)->where('type', $type)->first();
    }
}
