<?php
namespace App\Repositories;

use App\PersonalInformation;
use App\Repositories\ResponseRepository;
use App\Http\Resources\UserInfo;
use App\ClientDeviceToken;
use App\User;

class AuthUserRepository extends BaseRepository
{
    public function __construct(ResponseRepository $responseRepo, PersonalInformation $personalInfoModel,User $model)
    {
        $this->model = $model;
        $this->responseRepo =$responseRepo;
        $this->personalInfoModel = $personalInfoModel;
        $this->successStatusCode = config('http_status.success');
        $this->successMsg = config('message.successMsg');
        $this->resetPoint = config('enums.resetPoint');
        $this->firstStatus = config('enums.enableFirstStatus');
        $this->guestUserStatus = config('enums.userStatus.guest');
        $this->registeredUserStatus = config('enums.userStatus.registered');
    }

    public function checkFirstTimeUser($id)
    {
        return $this->model->where('id', $id)->where('first_status', $this->firstStatus)->first();
    }

    public function getUser()
    {
        $result['user'] = ['id'=> auth()->user()->id];
        return $this->responseRepo->successResponse($result, $this->successStatusCode, $this->successMsg);
    }

    public function getPersonalInfo($userId)
    {
        $userInfo = $this->personalInfoModel::where('client_user_id', $userId)
                                ->select('id', 'client_user_id', 'name', 'image')
                                ->first();
        $result['user']= new UserInfo($userInfo);

        return $this->responseRepo->successResponse($result, $this->successStatusCode, $this->successMsg);
    }

    public function upsertDeviceToken($clientUserId, $deviceId, $deviceToken, $deviceOsType, $userStatus)
    {
        if ($deviceId != null && $clientUserId != null) {

            $clientDevice = $this->checkDevice($deviceId);
            $registeredDevice = $this->checkDeviceByUserStatus($deviceId, $this->registeredUserStatus);
            $guestDevice = $this->checkDeviceByUserStatus($deviceId, $this->guestUserStatus);

            $data = [
                'client_user_id' => $clientUserId,
                'device_id' => $deviceId,
                'device_token' => $deviceToken,
                'user_status' => $userStatus,
                'device_os_type' => config("enums.osTypes.$deviceOsType"),
                'last_activated_at' => now()
            ];

            if (!$clientDevice || ($clientDevice && !$registeredDevice) || ($registeredDevice && !$guestDevice) ){
                $clientDevice = ClientDeviceToken::create($data);
            } elseif($registeredDevice && $guestDevice){
                $registeredDevice->update($data);
            }
           
            return $clientDevice->only('id');
        }
    }

    public function checkDevice($deviceId)
    {
        return ClientDeviceToken::where('device_id', $deviceId)->first();
    }

    public function checkDeviceByUserStatus($deviceId, $status)
    {
        return ClientDeviceToken::where('device_id', $deviceId)->where('user_status', $status)->first();
    }
}
