<?php
namespace App\Repositories;

use App\AppVersion;

class AppVersionRepository extends BaseRepository
{
	public function __construct(AppVersion $model)
	{
		$this->model = $model;
	}

	public function checkByVersionNameAndOsType($versionName,$osType)
	{
		$appVersion = $this->model->where('os_type',$osType)->orderBy('version_name','desc')->first();

		if($appVersion) {
			
			$appVersionPoints = explode(".",$appVersion->version_name);
			$requestedAppVersionPoints = explode(".",$versionName);
			
			if($requestedAppVersionPoints[0] <= $appVersionPoints[0] && $requestedAppVersionPoints[1] <= $appVersionPoints[1] && $requestedAppVersionPoints[2] < $appVersionPoints[2]) {
				return $appVersion;
			}
		}

		return null;
	}
}