<?php
namespace App\Repositories;

use App\User;
use App\PersonalInformation;

class ProfileRespository extends BaseRepository
{
    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
        $this->profileMediaDirectory = config('media.uaa.directory');
        $this->clientUserIdField = 'client_user_id';
        $this->imageField = 'image';
        $this->phoneField = 'phone';
        $this->emailField = 'email';
    }
   
    public function getById($id)
    {
        return $this->userModel->find($id);
    }

    public function clientPersonalInfo($clientUserId)
    {
        return PersonalInformation::where($this->clientUserIdField, $clientUserId)->first();
    }

    public function storeProfileMedia($media)
    {
        return  base64ImageUploadToAkoneyaMedia($media, $this->profileMediaDirectory);
    }

    public function updatePersonalProfileImage($clientUserId, $image)
    {
        $clientPersonalInfo = $this->clientPersonalInfo($clientUserId);

        $image_url = $this->storeProfileMedia($image);

        $clientPersonalInfo->update([
          $this->imageField => $image_url
        ]);

        return getFileUrlFromAkoneyaMedia($image_url);
    }

   
    public function updateClientPersonalInfo($clientUserId, $loginType, $request)
    {
        $clientPersonalInfo = $this->clientPersonalInfo($clientUserId);
      
        $data = [
          'name' => $request->name,
          'gender' => $request->gender,
          'date_of_birth' => $request->dateOfBirth,
          'bio' => $request->bio,
          'address' => $request->address,
        ];
        
    
        if ($loginType == config('login_types.phone')) {
            $data[$this->emailField] = $request->email;
        } elseif ($loginType == config('login_types.email')) {
            $data[$this->phoneField] = $request->phone;
        } else {
            $data[$this->emailField] = $request->email;

            $data[$this->phoneField] = $request->phone;
        }
        $clientPersonalInfo->update($data);
    }

    public function adminUserInfo()
    {
        return $this->userModel->where('is_admin', config('enums.isAdminUserStatus'))->first();
    }
}
