<?php
namespace App\Repositories;

use App\User;
use App\PersonalInformation;
use App\Repositories\UserRewardRepository;
use App\Repositories\ResponseRepository;
use App\Repositories\RegisterRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Carbon;
use Hash;
use JWTAuth;

use App\Http\Resources\Login as LoginResource;
use App\Http\Resources\PartnerLoginResource;

class LoginRepository extends BaseRepository
{
    public function __construct(User $userModel, UserRewardRepository $userRewardRepo,ResponseRepository $responseRepo, RegisterRepository $regRepo, BaseRepository $baseRepo,AuthUserRepository $authUserRepo)
    {
        $this->responseRepo =$responseRepo;
        $this->userModel = $userModel;
        $this->regRepo = $regRepo;
        $this->baseRepo = $baseRepo;
        $this->authUserRepo = $authUserRepo;
        $this->userRewardRepo = $userRewardRepo;
        $this->badRequestStatusCode = config('http_status.badRequest');
        $this->notFoundStatusCode = config('http_status.notFound');

        $this->successStatusCode = config('http_status.success');
        $this->successMsg = trans('message.successMsg');
       
        $this->notFoundMsg = config('message.notFoundMsg');
        $this->phoneLoginFail = config('message.phoneLoginFail');
        $this->EmailLoginFail = config('message.EmailLoginFail');

        $this->placeholder_image_url = config('mock_data.fakeProfile');
        $this->accessTokenfield = 'accessToken';
        $this->disableFirstStatus = config('enums.disableFirstStatus');
        $this->isPartnerUserStatus = config('enums.isPartnerUserStatus');
        $this->disableBannedStatus = config('enums.disableBannedStatus');
        $this->guestUserStatus = config('enums.userStatus.guest');
        $this->registeredUserStatus = config('enums.userStatus.registered');
    }

    public function loginWithPhoneOrEmail($request)
    {
        $data = $request->all();
        $data[$this->baseRepo->emailOrPhoneField] = isset($data[$this->baseRepo->phoneField]) ? $data[$this->baseRepo->phoneField] :  $data[$this->baseRepo->emailField];
        $data['type'] = $data[$this->baseRepo->loginTypeField];
        $userData = $this->regRepo->checkRegister($data);
        if (empty($userData)) {
            return $this->responseRepo->failResponse(null, $this->successStatusCode, trans('message.notFoundMsg'));
        }
        
        if ($data[$this->baseRepo->loginTypeField] == $this->baseRepo->phoneField) {
            $login = [
                    $this->baseRepo->phoneField => $data[$this->baseRepo->phoneField],
                    $this->baseRepo->passwordField => $data[$this->baseRepo->passwordField]
            ];
        } else {
            $login = [
                    $this->baseRepo->emailField => $data[$this->baseRepo->emailField],
                    $this->baseRepo->passwordField => $data[$this->baseRepo->passwordField]
            ];
        }
        if (! $token = auth('api')->attempt($login)) {
            return $this->responseRepo->failResponse(null, $this->successStatusCode, $data[$this->baseRepo->loginTypeField] == $this->baseRepo->phoneField ? trans('message.phoneLoginFail') :  trans('message.EmailLoginFail'));
        }
      
        $accessToken = toEncryptToken($token);

        $data = ['user'=>$this->getUserInfo($userData->id),$this->accessTokenfield => $accessToken];

        $this->baseRepo->updateFirstStatusForInitUser($userData);

        $this->authUserRepo->upsertDeviceToken($userData->id,$request->deviceId,$request->deviceToken,$request->deviceOsType, $this->registeredUserStatus);

        return $this->responseRepo->successResponse($data, $this->successStatusCode, $this->successMsg);
    }
    
    public function socialLogin($request)
    {
        $data = $request->all();
        /*check provider id*/
        
        switch ($data[$this->baseRepo->loginTypeField]) {
            case $this->baseRepo->facebook:
                        $providerId = 'facebook_id';
                        $requestProviderId = $data['facebookId'];
                        break;
            case $this->baseRepo->apple:
                        $providerId = 'apple_id';
                        $requestProviderId = $data['appleId'];
                        break;
            default:
                        $providerId = 'email_id';
                        $requestProviderId = $data['emailId'];

        }
        $checkUserWithProviderId = $this->userModel::where($providerId, $requestProviderId)
                            ->where('active_status', '=', $this->baseRepo->enableActiveStatus)
                            ->orderby('id', 'desc')
                            ->first();
 
        $existingDevice = $this->authUserRepo->checkDeviceByUserStatus($request->deviceId, $this->guestUserStatus);

        //New User
        if (!$checkUserWithProviderId) {

            if ($data[$this->baseRepo->loginTypeField] != $this->baseRepo->facebook) {
                $checkUser = $this->userModel::where($this->baseRepo->emailField, $data[$this->baseRepo->emailField])
                                ->where('active_status', '=', $this->baseRepo->enableActiveStatus)
                                ->orderby('id', 'desc')
                                ->first();
            }
            if (isset($checkUser)) {
                switch ($data[$this->baseRepo->loginTypeField]) {
                    case $this->baseRepo->facebook:
                        $checkUser->update([
                            'facebook_id' => $data['facebookId']
                        ]);
                        break;
                    case $this->baseRepo->apple:
                        $checkUser->update([
                            'apple_id' => $data['appleId']
                        ]);
                        break;
                    default:
                        $checkUser->update([
                            'email_id' => $data['emailId']
                        ]);
                }
                $token = JWTAuth::fromUser($checkUser);
                $accessToken =  toEncryptToken($token);

                $data['type']=$data[$this->baseRepo->loginTypeField];

                $this->baseRepo->cloneToProfile($data, $checkUser->id);

                $result = ['user'=>$this->getUserInfo($checkUser->id),$this->accessTokenfield =>$accessToken];

                $this->baseRepo->updateFirstStatusForInitUser($checkUser);

            } else { 
                $reward = $this->userRewardRepo->getFirstTimeReward(config('enums.rewardType.firstRegister'));
                $data['coin'] = $reward->reward_coin;
            
                if ($existingDevice){
                    $guestUser =  $this->userModel->where('id', $existingDevice->client_user_id)->where('user_type',config('enums.guestUser'))->first();
                    if($guestUser){
                        $data['coin'] = $reward->reward_coin + $guestUser->coin;
                     }
                }
                $user = $this->baseRepo->socialUserCreate($data);

                $token = JWTAuth::fromUser($user);
                $accessToken =  toEncryptToken($token);

                $data['type'] = $data[$this->baseRepo->loginTypeField];
                $this->baseRepo->cloneToProfile($data, $user->id);
                $result = ['user'=>$this->getUserInfo($user->id),$this->accessTokenfield => $accessToken];
                $this->baseRepo->updateFirstStatusForInitUser($user);
            }
        } else {
            if($checkUserWithProviderId->banned_status != $this->disableBannedStatus ){
                return $this->responseRepo->failResponse(null,config('http_status.badRequest'), trans('message.notFoundMsg'));
            }
            $data['type'] = $data[$this->baseRepo->loginTypeField];
            $this->baseRepo->cloneToProfile($data, $checkUserWithProviderId->id);
            $token = JWTAuth::fromUser($checkUserWithProviderId);
            $accessToken = toEncryptToken($token);
            $result = ['user'=>$this->getUserInfo($checkUserWithProviderId->id),$this->accessTokenfield =>$accessToken];

            $this->baseRepo->updateFirstStatusForInitUser($checkUserWithProviderId);
        }

        $this->authUserRepo->upsertDeviceToken($result['user']['id'],$request->deviceId,$request->deviceToken,$request->deviceOsType, $this->registeredUserStatus);

        return $this->responseRepo->successResponse($result, $this->successStatusCode, $this->successMsg);
    }

    public function getUserInfo($userId)
    {
        $clientUserId = 'client_users.id';
        $userInfo = User::join('personal_information', 'personal_information.client_user_id', '=', $clientUserId)
                            ->select(
                                'client_users.id',
                                'personal_information.name',
                                'personal_information.email',
                                'personal_information.image',
                                'personal_information.address',
                                'client_users.login_type',
                                'client_users.phone',
                                'client_users.first_status', 
                                'client_users.coin'
                            )
                            ->where($clientUserId, $userId)
                            ->first();

        $reward = $this->userRewardRepo->getFirstTimeReward(config('enums.rewardType.firstRegister'));

        return  new LoginResource($userInfo, $reward->reward_coin);
    }

    public function validationForPasswordUser($authId)
    {
        return $this->userModel->where('id',$authId)->where('active_status','active')->first();
    }


    public function validationForPhone($phone)
    {
        return $this->userModel->select('phone')->where('phone',$phone)->where('active_status','active')->pluck('phone');
    }

}
