<?php
namespace App\Repositories;

use App\User;
use App\Repositories\ResponseRepository;
use Illuminate\Support\Facades\Hash;

class ChangePasswordRepository extends BaseRepository
{
    public function __construct(ResponseRepository $responseRepo, User $userModel)
    {
        $this->responseRepo = $responseRepo;
        $this->userModel = $userModel;
        $this->successStatusCode = config('statuscode.successStatusCode');
        $this->badRequestStatusCode = config('statuscode.badRequestStatusCode');
        $this->unauthorizedStatusCode = config('http_status.unauthorized');

        $this->activeStatusField = 'active_status';
    }
    public function changePassword($data, $userId)
    {
        $statusCode = $this->badRequestStatusCode;
        $message = null;

        if ($data['newPassword'] != $data['confirmNewPassword']) {

            $message = trans('message.passwordNotMatch');

        } elseif (!check_auth_user_id($userId, $data['id'])) {

            $statusCode = $this->unauthorizedStatusCode;
            $message = trans('message.tokenInvalidOrExpire');

        } else {
            $user = $this->userModel::where('id', $data["id"])->where('active_status', "active")->first();

            if (empty($user)) {

                $message = trans('message.notFoundMsg');

            } elseif (!Hash::check($data['currentPassword'], $user->password)) {

                $message = trans('message.currentPasswordFail');

            } else {

                $user->update(['password'=>bcrypt($data['newPassword'])]);
                return $this->responseRepo->successResponse(null, $this->successStatusCode, trans('message.successChangePassword'));
            }
        }
        return $this->responseRepo->failResponse(null, $statusCode, $message);
    }
}
