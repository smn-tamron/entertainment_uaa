<?php
namespace App\Repositories;

use App\User;
use App\Repositories\ResponseRepository;
use App\Services\MailService;

class ForgetPasswordRepository extends BaseRepository
{
    public function __construct(ResponseRepository $ResponseRepository, User $userModel, MailService $mailService, BaseRepository $baseRepo)
    {
        $this->baseRepo = $baseRepo;

        $this->ResponseRepository =$ResponseRepository;
        $this->userModel =$userModel;
        $this->successStatusCode = config('statuscode.successStatusCode');
        $this->notFoundStatusCode = config('statuscode.notFoundStatusCode');

        $this->mailSubject = config('message.mailSubject');
        $this->mailBody = config('message.mailBody');
        $this->successChangePassword  = config('message.successChangePassword');
        $this->mailService = $mailService;
    }

    public function successForgetPassword($emailOrPhone)
    {
        return $this->ResponseRepository->successResponse([$this->baseRepo->otpSend=>true], $this->successStatusCode, trans('message.forgetPasswordSuccess'));
    }

    public function getUser($emailOrPhone)
    {
        return $this->userModel::where('phone', $emailOrPhone)
                                                ->orwhere($this->baseRepo->emailField, $emailOrPhone)
                                                ->where($this->baseRepo->activeStatusField, $this->baseRepo->enableActiveStatus)->first();
    }
  
    public function forgetPassword($data)
    {
        $getUserInfo = $this->getUser($data[$this->baseRepo->emailOrPhoneField]);

        if ($getUserInfo == null) {
            return $this->ResponseRepository->successResponse([$this->baseRepo->isRegistered => false], $this->successStatusCode, trans('message.accountExist'));
        } else {
            if ($this->checkemail($data[$this->baseRepo->emailOrPhoneField])) {
                $otpCode =  $this->baseRepo->generateNumericOTP(6);
                $otpAttemptTime = date('Y-m-d H:i:s');
                $data['user_id'] = $getUserInfo->id;
                $data['type'] =  $getUserInfo->login_type;
                $data['is_forget'] = "true";
                $data['emailOrPhone'] = $getUserInfo->email;
                $data['otp_code'] = $otpCode;
                $data['otp_attempt_time'] = $otpAttemptTime;
                $this->baseRepo->toTempRegister($data);
                $this->mailService->phpMailer($data[$this->baseRepo->emailOrPhoneField], $this->mailSubject, $this->mailBody.$otpCode);

                $this->userModel::where($this->baseRepo->emailField, $data[$this->baseRepo->emailOrPhoneField])->update(['otp_code'=> $otpCode,'otp_attempt_time'=> $otpAttemptTime]);
            }
            else{
                $data['user_id'] = $getUserInfo->id;
                $data['type'] = $getUserInfo->login_type;
                $data['is_forget'] = "true";
                $data['emailOrPhone'] = $getUserInfo->phone;
                $data['otp_code'] = null;
                $data['otp_attempt_time'] = null;
                $this->baseRepo->toTempRegister($data);
            }

            return $this->ResponseRepository->successResponse([$this->baseRepo->isRegistered => true], $this->successStatusCode, trans('message.successMsg'));
        }
    }
    public function createNewPassword($data)
    {
        $checkEmailOrPhone =  $this->getUser($data[$this->baseRepo->emailOrPhoneField]);

        if ($checkEmailOrPhone == null || $data['otpUser'] !=  $checkEmailOrPhone->id) {
            return $this->ResponseRepository->failResponse(null, $this->notFoundStatusCode, trans('message.notFoundMsg'));
        }
        if ($this->checkemail($data[$this->baseRepo->emailOrPhoneField])) {
            $this->userModel::where($this->baseRepo->emailField, $data[$this->baseRepo->emailOrPhoneField])->update([$this->baseRepo->passwordField=>bcrypt($data[$this->baseRepo->passwordField]),'login_type'=>$this->baseRepo->emailField]);
        } else {
            $this->userModel::where($this->baseRepo->phoneField, $data[$this->baseRepo->emailOrPhoneField])->update([$this->baseRepo->passwordField=>bcrypt($data[$this->baseRepo->passwordField]),'login_type'=>$this->baseRepo->phoneField]);
        }
        
        return $this->ResponseRepository->successResponse(null, $this->successStatusCode, trans('message.successChangePassword'));
    }


    public function getEmailAndPhoneByuserId($id)
    {
        return $this->userModel::select('email','phone')->where('id',$id)->first();
    }
}
