<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardType extends Model
{
    protected $table = "reward_types";
    protected $guarded = [];
}
