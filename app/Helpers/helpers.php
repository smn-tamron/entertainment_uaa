<?php

use App\ExpLevel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\File;

/**
 * Upload File To Akoneya Media
 */

if (!function_exists('base64ImageUploadToAkoneyaMedia')) {
    function base64ImageUploadToAkoneyaMedia($base64String, $directoryName)
    {
        $file = base64_decode($base64String);
        $fileExtension = ".png";
        $fileName = time().Str::random(6).$fileExtension;
        $mediaPath = $directoryName."/".$fileName;
        if (Storage::put($mediaPath, $file)) {
            return $mediaPath;
        }
        return null;
    }
}

if (!function_exists('socialImageUploadToAkoneyaMedia')) {
    function socialImageUploadToAkoneyaMedia($url, $directoryName)
    {
        $file = file_get_contents($url);
        $fileExtension = ".png";
        $fileName = time().Str::random(6).$fileExtension;
        $mediaPath = $directoryName."/".$fileName;
        if (Storage::put($mediaPath, $file)) {
            return $mediaPath;
        }
        return null;
    }
}
/**
 * Delete File From Akoneya Media
 */
if (!function_exists('deleteFileFromAkoneyaMedia')) {
    function deleteFileFromAkoneyaMedia($filePath)
    {
        Storage::delete($filePath);
    }
}
/*
 * Get File URL from Akoneya Medai
 */
if (!function_exists('getFileUrlFromAkoneyaMedia')) {
    function getFileUrlFromAkoneyaMedia($filePath)
    {
        $fileUrl = Storage::url($filePath);
        return $fileUrl;
    }
}
/**
 * Upload Base64 Image
 */

if (!function_exists('base64_to_image')) {
    function base64_to_image($base64String, $prefix)
    {
        $photo = base64_decode($base64String);

        $photo_name= $prefix.time().Str::random(6). '.jpg';

        Storage::put($photo_name, $photo);

        return $photo_name;
    }
}

/**
 * Get Storage File
 */

if (! function_exists('get_storage_file_path')) {
    function get_storage_file_path($file = null)
    {
        return Storage::url($file);
    }
}

if (!function_exists("decryptToken")) {
    function decryptToken($token)
    {
        if (!defined('AES_256_CBC')) {
            define('AES_256_CBC', 'aes-256-cbc');
        }

        $encryption_key = Config::get('services.encryptionKey');

        $iv = Config::get('services.ivKey');

        return openssl_decrypt($token, AES_256_CBC, $encryption_key, 0, $iv);
    }
}

/**
* Check Request Auth User Id
*
*/

if (!function_exists('check_auth_user_id')) {
    function check_auth_user_id($authUserId, $passedId)
    {
        return $authUserId == $passedId;
    }
}

if (!function_exists("checkBased64ImageSizeLimit")) {
    function checkBased64ImageSizeLimit($value)
    {
        $imageSize = (int) (strlen($value) * 3 / 4) / 1024;
        if ($imageSize > config('filesystems.imageSizeLimit')) {
            return false;
        }
        return true;
    }
}


    function toEncryptToken($token)
    {
        if (!defined('AES_256_CBC')) {
            define('AES_256_CBC', 'aes-256-cbc');
        }

        $encryption_key = Config::get('services.encryptionKey');

        $iv = Config::get('services.ivKey');

        $encrypted = openssl_encrypt($token, AES_256_CBC, $encryption_key, 0, $iv);

        $decryptedToken = openssl_decrypt($encrypted, AES_256_CBC, $encryption_key, 0, $iv);

        // return $decryptedToken;
        return $encrypted;
    }

    if (!function_exists('strTo24hrFormat')) {
        function strTo24hrFormat($value)
        {
            return date('Y-m-d H:i:s', strtotime($value));
        }
    }

    if (!function_exists('changeLanguage')) {
        function changeLanguage($request, $en, $mm, $zh)
        {
            $headerLanguage = $request->header('Content-Language');
    
            if ($headerLanguage == 'mm' && $mm != null) {
                $lang = $mm;
            } elseif ($headerLanguage == 'zh' && $zh != null) {
                $lang = $zh;
            } else {
                $lang = $en;
            }
    
            return $lang;
        }
    }

    if (!function_exists('getCurrentExpLevel')) {
        function getCurrentExpLevel($exp)
        {
            $expLevels = ExpLevel::orderBy('id', 'asc')->get();
            foreach ($expLevels as $level) {
                if($exp >= $level['from_exp_point'] && $exp <= $level['to_exp_point'])
                {
                    return $level;
                }
            }
            return ExpLevel::latest()->orderBy('id', 'desc')->first();
        }
    }

    if (!function_exists('getNextExpLevel')) {
        function getNextExpLevel($exp)
        {
            $last = ExpLevel::where('target_exp_point', '>=' , $exp)->first();
            return $last;
        }
    }

    if (!function_exists('getLevelThreshold')) {
        function getCurrentLevelExp($exp)
        {
            $current = ExpLevel::where('target_exp_point' , '<=' , $exp)->latest()->orderBy('id', 'desc')->first();
            if($current == null)
            {
                return 0;
            } else {
                return $current->target_exp_point;
            }
        }
    }

    if (!function_exists('getLevelThreshold')) {
        function getNextLevelExp($exp)
        {
            $last = ExpLevel::where('target_exp_point', '>=' , $exp)->first();
            return $last->target_exp_point;
        }
    }

    if (!function_exists('getMemberShipLevel')) {
        function getMemberShipLevel($currentLevel)
        {
            $levels = config('member_levels.memberLevels');
            foreach ($levels as $level) {
               if($currentLevel >= $level['from'] && $currentLevel <= $level['to'])
               {
                   return $level;
               }
            }
        }
    }
