<?php

namespace App\Exceptions;


use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Exception;
use ErrorException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\QueryException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }
    

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {    
        if ($exception instanceof UnauthorizedHttpException) {
            $preException = $exception->getPrevious();
            if ($preException instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException)
            {
                return response()->json([
                    'result' => null,
                    'statusCode' => 401,
                    'message' => 'Token is expired.'
                ]);
            }
            else if ($preException instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
            {
                return response()->json([
                    'result' => null,
                    'statusCode' => 401,
                    'message' => 'Token is Invalid.'
                ]);
            }
            else if ($preException instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {

                return response()->json([
                    'result' => null,
                    'statusCode' => 401,
                    'message' => 'The token has been blacklisted.'
                ]);
            }
        }

        if ($exception->getMessage() === 'Token not provided')
        {
            return response()->json([
                'result' => null,
                'statusCode' => 401,
                'message' => 'Token not provided.'
            ]);
            
        }

        if ($exception instanceof NotFoundHttpException){
            return response()->json([
                'result' => null,
                'statusCode' => 404,
                'message' => 'Not Found.'
            ],Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof HttpException){
            return response()->json([
                'result' => null,
                'statusCode' => $exception->getStatusCode(),
                'message' => $exception->getMessage()
            ],$exception->getStatusCode());
        }


        if ($exception instanceof QueryException) 
        {
            return response()->json([
                'result' => null,
                'statusCode' => 500,
                'message' => 'Internal Server Error.',
            ], 500);          
        }

        if ($exception instanceof ErrorException) 
        {
            return response()->json([
                'result' => null,
                'statusCode' => 500,
                'message' => 'Internal Server Error.',
            ], 500);          
        }

        return parent::render($request, $exception);
    }
 
}
