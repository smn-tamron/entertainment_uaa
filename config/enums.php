<?php

return [
    'akoneyaMediaFolder' => 'akoneya',
    'enableFirstStatus' => 1,
    'disableFirstStatus' => 0,
    'enablePublishStatus' => 1,
    'enableActiveStatus' => 'active',
    'disableDeletedStatus' => 0,
    'enableDeletedStatus' => 1,
    'disableBannedStatus' => 0,
    'pendingActiveStatus' => 'pending',
    'otpVerified' => 'isVerified',
    'otpSend' => 'isSend',
    'otpVerifySuccess' => true,
    'otpVerifyFail' => false,
    'otpSendSuccess'=> true,
    'otpSendFail' => false,
    'isRegistered' => 'isRegistered',
    "fakeProfile" => "games/userProfiles/fake-profile.png",
    'osTypes' => [
        'android' => 1,
        'ios' => 2
    ],
    'gameCategory' => [
        "covid" => 1,
        "slotReel" => 2,
    ],
    'rewardType' => [
        "guest" => 'guest',
        "firstRegister" =>'newbie'
    ],
    'imageTypes' => [
        1 => 'image',
        2 => 'video',
    ],
    'perPage' => 10,
    'isAdminUserStatus' => 1,
    'guestUser'=> 'guest',
    'registeredUser'=> 'registered',
    'userStatus' => [
        "guest" => 0,
        "registered" => 1,
    ],
];
