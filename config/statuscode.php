<?php
return[
    'createStatusCode' => 201,
    'successStatusCode' => 200,
    'badRequestStatusCode' => 400,
    'notFoundStatusCode' => 404,
];
