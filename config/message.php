<?php

return [
    
    'alreadyRegister' => 'Your account is already registered!',
    'successRegister' => 'Your account is succefully registered.',
    'successOtpSend' => 'Otp code is successfully sent.',
    'mailSubject' => 'Akoneya',
    'mailBody' => 'Your otp code is ',
    'otpSuccess' => 'Verified successfully.',
    'invalidOtp' => 'Invalid your otp.',
    'successMsg' => 'Success',
    'notFoundMsg' => 'No record.',
  
    'currentPasswordFail' => 'Invalid current password.',
    'successChangePassword' => 'Changed password successfully.',
    'otpExpired' => 'Your otp is expire.Try again.',
    'phoneLoginFail' => 'Incorrect phone or password.',
    'EmailLoginFail' => 'Incorrect email or password.',
    'accountExist' => 'This account does not exist.',
    'successLogout' => 'Logout successfully.',
    'unauthorised'=>'Unauthorized',
    'registerAllFieldValidationFail' => 'The type field is required.The name field is required.The email or phone field is required.The password field is required.The password confirmation field is required.',
    'otpSendValidationFail' => 'The email field is required.',
    'otpVerifyValidationFail' => 'The email or phone field is required.The otp code field is required.',
    'loginValidationFail' => 'The name field is required.The email field is required.The apple id field is required.',
    'profileUpdateValidationFail' => 'The name field is required.The gender field is required.',
    'profileUploadValidationFail' => 'The id field is required.The image field is required.',


];
